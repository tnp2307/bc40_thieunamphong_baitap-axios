const BASE_URL = "https://63b2c99e5901da0ab36dbae0.mockapi.io";
function renderFoodList(foods) {
  var contentHTML = "";
  foods.reverse().forEach(function (item) {
    var contentTr = `<tr>
        <td>${item.maMon}</td>
        <td>${item.tenMon}</td>
        <td>${item.giaMon}</td>
        <td>${
          item.loaiMon
            ? "<span class='text-primary'>Mặn</span>"
            : "<span class='text-success'>Chay</span>"
        }
        </td>
        <td>${convertString(20, item.hinhAnh)}</td>
        <td>
         <button onclick="xoaMon('${
           item.maMon
         }')" class="btn btn-danger">Xóa</button> 
         <button class="btn btn-warning" onclick="suaMonAN('${
           item.maMon
         }')">Sửa</button> 
        </td>
        </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
}
function fetchFoodList() {
  batSpinner();
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then(function (res) {
      // xử lý khi gọi API thành công
      tatSpinner();
      var foodList = res.data;
      renderFoodList(foodList);
    })
    .catch(function (err) {
      // xử lý khi gọi API thất bại
      console.log(err);
    });
}

// gọi API lấy danh sách món ăn từ server
fetchFoodList();

// xóa món
function xoaMon(id) {
  batSpinner();
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      // gọi lại API sau khi xóa thành công
      tatSpinner();
      fetchFoodList();
    })
    .catch(function (err) {
      console.log(err);
    });
}

// them mon an
function themMonAn() {
  var monAn = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data: monAn,
  })
    .then(function (res) {
      fetchFoodList();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function suaMonAN(id) {
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatSpinner();
      document.getElementById("maMon").disabled = true;
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatSpinner();
    });
}

function capNhatMonAn() {
  batSpinner();
  var monAn = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/food/${monAn.maMon}`,
    method: "PUT",
    data: monAn,
  })
    .then(function (res) {
      tatSpinner();
      fetchFoodList();
    })
    .catch(function (err) {
      tatSpinner();
    });
}

// promise all, promise chaining
//
axios({
  url: "https://63b2c99e5901da0ab36dbae0.mockapi.io/food",
  method: "GET",
})
  .then(function (res) {
    // xử lý khi gọi API thành công
    var foodList = res.data;
    renderFoodList(foodList);
  })
  .catch(function (err) {
    // xử lý khi gọi API thất bại
    console.log(err);
  });
