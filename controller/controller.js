function batSpinner() {
  document.getElementById("spinner").style.display = "flex";
}
function tatSpinner() {
  document.getElementById("spinner").style.display = "none";
}

function layThongTinTuForm() {
  const maMon = document.getElementById("maMon").value;
  const tenMon = document.getElementById("tenMon").value;
  const giaMon = document.getElementById("giaMon").value;
  const hinhAnh = document.getElementById("hinhAnh").value;
  const loaiMon = document.getElementById("loaiMon").value;

  var monAn = {
    maMon: maMon,
    tenMon: tenMon,
    giaMon: giaMon,
    hinhAnh: hinhAnh,
    loaiMon: loaiMon,
  };
  return monAn;
}

function convertString(maxLength, value) {
  if (value.length > maxLength) {
    return value.slice(0, maxLength) + "...";
  } else {
    return value;
  }
}
function showThongTinLenForm(food) {
  document.getElementById("maMon").value = food.maMon;
  document.getElementById("tenMon").value = food.tenMon;
  document.getElementById("giaMon").value = food.giaMon;
  document.getElementById("hinhAnh").value = food.hinhAnh;
  document.getElementById("loaiMon").value = food.loaiMon;
}
